const puppeteer = require('puppeteer');
const url = 'http://localhost:3000';
const openBrowser = true;

// customized delay
function delay(timeout) {
    return new Promise((resolve) => {
        setTimeout(resolve, timeout);
    });
}

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
    await delay(500);
    return;
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let join_button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(join_button).toBe('Join');
    await browser.close();
    await delay(500);
    return;
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await delay(500);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
    await delay(500);
    return;
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1'  , {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await delay(1000);
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
    await delay(500);
    return;
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.keyboard.press('Enter', {delay: 100});
    await delay(750);
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
    await delay(500);
    return;
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1'  , {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitForSelector('#swal2-title');
    let welcome_title = await page.evaluate(() => {
        return document.querySelector('#swal2-title').innerHTML;
    });
    expect(welcome_title).toBe('Welcome!');
    await page.waitForSelector('#messages > li > div.message__body > p');
    let welcome_message = await page.evaluate(() => {
        return document.querySelector('#messages > li > div.message__body > p').innerHTML;
    });
    expect(welcome_message).toBe('Hi John, Welcome to the chat app');
    await browser.close();
    await delay(500);
    return;
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');
    await browser.close();
    await delay(500);
    return;
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');
    await browser.close();
    await delay(500);
    return;
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    await browser.close();
    await delay(500);
    return;
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage();
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page1.goto(url);
    await page2.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#users > ol > li:nth-child(1)');
    let b1_member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page1.waitForSelector('#users > ol > li:nth-child(2)');
    let b1_member2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    await page2.waitForSelector('#users > ol > li:nth-child(1)');
    let b2_member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page2.waitForSelector('#users > ol > li:nth-child(2)');
    let b2_member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(b1_member1).toBe('John');
    expect(b1_member2).toBe('Mike');
    expect(b2_member1).toBe('John');
    expect(b2_member2).toBe('Mike');
    await browser1.close();
    await browser2.close();
    await delay(500);
    return;
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    let send_button = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send_button).toBe('Send');
    await browser.close();
    await delay(500);
    return;
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    await page.type('#message-form > input[type=text]', 'testing.', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__title > h4');
    let speaking = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__title > h4').innerHTML;
    });
    expect(speaking).toBe('John');
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');
    let test_message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__body > p').innerHTML;
    });
    expect(test_message).toBe('testing.');
    await browser.close();
    await delay(500);
    return;
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser1.newPage();
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page1.goto(url);
    await page2.goto(url);
    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#users > ol > li:nth-child(1)');
    let b1_member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page1.waitForSelector('#users > ol > li:nth-child(2)');
    let b1_member2 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    await page2.waitForSelector('#users > ol > li:nth-child(1)');
    let b2_member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page2.waitForSelector('#users > ol > li:nth-child(2)');
    let b2_member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(b1_member1).toBe('John');
    expect(b1_member2).toBe('Mike');
    expect(b2_member1).toBe('John');
    expect(b2_member2).toBe('Mike');
    // John says "Hi"
    await page1.type('#message-form > input[type=text]', 'Hi', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#messages > li:nth-child(3) > div.message__title > h4');
    let b1_speaking1 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div.message__title > h4').innerHTML;
    });
    expect(b1_speaking1).toBe('John');
    await page1.waitForSelector('#messages > li:nth-child(3) > div.message__body > p');
    let b1_test_message1 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div.message__body > p').innerHTML;
    });
    expect(b1_test_message1).toBe('Hi');
    await page2.waitForSelector('#messages > li:nth-child(2) > div.message__title > h4');
    let b2_speaking1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__title > h4').innerHTML;
    });
    expect(b2_speaking1).toBe('John');
    await page2.waitForSelector('#messages > li:nth-child(2) > div.message__body > p');
    let b2_test_message1 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__body > p').innerHTML;
    });
    expect(b2_test_message1).toBe('Hi');
    // Mike says "Hello"
    await page2.type('#message-form > input[type=text]', 'Hello', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    await page1.waitForSelector('#messages > li:nth-child(4) > div.message__title > h4');
    let b1_speaking2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div.message__title > h4').innerHTML;
    });
    expect(b1_speaking2).toBe('Mike');
    await page1.waitForSelector('#messages > li:nth-child(4) > div.message__body > p');
    let b1_test_message2 = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div.message__body > p').innerHTML;
    });
    expect(b1_test_message2).toBe('Hello');
    await page2.waitForSelector('#messages > li:nth-child(3) > div.message__title > h4');
    let b2_speaking2 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div.message__title > h4').innerHTML;
    });
    expect(b2_speaking2).toBe('Mike');
    await page2.waitForSelector('#messages > li:nth-child(3) > div.message__body > p');
    let b2_test_message2 = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div.message__body > p').innerHTML;
    });
    expect(b2_test_message2).toBe('Hello');
    await browser1.close();
    await browser2.close();
    await delay(500);
    return;
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    let location_button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(location_button).toBe('Send location');
    await browser.close();
    await delay(500);
    return;
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    const client = await page.target().createCDPSession();
    // emulate location
    await client.send('Emulation.setGeolocationOverride', {
        latitude: 25.017905,
        longitude: 121.539014,
        accuracy: 17,
    });
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');
    await page.$eval('#send-location', (content) => content.click());
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__title > h4');
    let speaking = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div.message__title > h4').innerHTML;
    });
    expect(speaking).toBe('John');
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > div > iframe');
    await browser.close();
    await delay(500);
    return;
})
