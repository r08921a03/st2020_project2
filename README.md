# [Project 2 - GUI testing](https://hackmd.io/@xlYUTygoRkyuQQlwXuWDWQ/r1UU-rOL8)

Student ID: r08921a03

<hr />

## Get Started

* `git clone https://gitlab.com/r08921a03/st2020_project2.git`
* `cd st2020_project2`
* `yarn install`
* `yarn start`
    * visit your web application at http://localhost:3000
* Open another terminal, and run `yarn test`.
* Create a new public repository called `st2020_project2` and change remote.


## TODO 

Complete the 15 test cases in the `test/ui.test.js`, the screenshot should be  stored in the `test/screenshots/` folder.

1. [Content] Login page title should exist
2. [Content] Login page join button should exist
3. [Screenshot] Login page
4. [Screenshot] Welcome message
    * <img src="https://i.imgur.com/3X8uyYB.png" width="50%" />
5. [Screenshot] Error message when you login without user and room name
    * <img src="https://i.imgur.com/6RGIPoD.png" width="50%" />
6. [Content] Welcome message
7. [Behavior] Type user name
8. [Behavior] Type room name
9. [Behavior] Login
10. [Behavior] Login 2 users
11. [Content] The "Send" button should exist
12. [Behavior] Send a message
13. [Behavior] John says "Hi" and Mike says "Hello"
14. [Content] The "Send location" button should exist
15. [Behavior] Send a location message

